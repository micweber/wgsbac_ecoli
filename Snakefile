import pandas as pd
import os


##from folder.A import *

configfile: "config.yaml"

## Read the config metadata
meta_table = pd.read_table(config["metadata"]).set_index("ID",drop=False)

## Read the config samples
samples = pd.read_table(config["samples"]).set_index("ID",drop=False)

## Check which sample IDs have a Null value
samples_fastq = samples[~samples["File_left_reads"].isnull().values]
samples_fasta = samples[~samples["File_assembly"].isnull().values]

## Link the samples_fasta files
os.makedirs("results/finalAssembly", exist_ok=True)
for row in samples_fasta.itertuples():
  src = row.File_assembly
  print(src)
  dst = "results/finalAssembly/" + row.ID + ".fasta"
  if not os.path.exists(dst):
    os.symlink(src, dst)
    


## Define snakemake rules
#include: "rules/link_input.smk"
include: "rules/coverage_V2.smk"
include: "rules/fastqc.smk"
include: "rules/shovill.smk"
#include: "rules/spades.smk"
include: "rules/quast.smk"
include: "rules/prokka.smk"
include: "rules/multiqc.smk"
include: "rules/abricate.smk"
include: "rules/amrfinderplus.smk"
include: "rules/trimquality.smk",
include: "rules/serotypefinder.smk"
include: "rules/clermont.smk"
include: "rules/mlst.smk"
include: "rules/ksnp.smk"
#include: "rules/snippy.smk"
include:  "rules/itol.smk"


rule all:
  input:
    expand("input/{sample.ID}_R1.fastq.gz",sample=samples_fastq.itertuples()),
    expand("input/{sample.ID}_R2.fastq.gz",sample=samples_fastq.itertuples()),
    expand("results/shovill/{sample.ID}",sample=samples_fastq.itertuples()),
    expand("results/finalAssembly/{sample.ID}.fasta",sample=samples_fasta.itertuples()),
    "results/quast/allquast.csv",
	  "results/quast/quast",
	  expand("results/prokka/{sample.ID}",sample=samples.itertuples()),
	  "results/coverage/Coverage_fastq_info_mqc.txt",
	  "results/multiqc_fastqc",
	  "results/multiqc",
	  "results/trees/kSNP_NJ_tree.pdf",
	  "results/trees/kSNP_ML_tree.pdf",
	  "results/trees/kSNP_parsimoney_tree.pdf",
	  "results/itol/itol_finished"


