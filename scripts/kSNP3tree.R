library(ggtree)
library(ggplot2)
library(treeio)


folder<- snakemake@input[["ksnp_res"]]#result folder form ksnp
meta=read.csv(snakemake@input[["metadata"]],sep="\t", stringsAsFactors = FALSE)#reads metadata
dd = as.data.frame(meta)
cat("read medatada \n",file="log/ksnp/drawkSNPtree.log",sep="")
pars=read.tree(paste(folder,"tree.parsimony.tre",sep="/"))
nj=read.tree(paste(folder,"tree.NJ.tre",sep="/"))
ml=read.tree(paste(folder,"tree.ML.tre",sep="/"))
cat("read trees \n",file="log/ksnp/drawkSNPtree.log",sep="",append=TRUE)

  #change tip label from ID to common nam
for(j in 1:length(pars$tip.label)) pars$tip.label[j]=meta[meta[,1]==pars$tip.label[j],2]#changes tip labels
for(j in 1:length(nj$tip.label)) nj$tip.label[j]=meta[meta[,1]==nj$tip.label[j],2]#changes tip labels
for(j in 1:length(ml$tip.label)) ml$tip.label[j]=meta[meta[,1]==ml$tip.label[j],2]#changes tip labels

tr=pars
pdf(snakemake@output[["partree"]])#creates file
  p <- ggtree(tr) +
   xlim(0, 15) + # to allow more space for labels
   ggtitle("parsimoney tree from ksnp")#prints title
   p %<+% dd +  geom_tiplab(aes(fill = Type),
               color = "black", # color for label font
               geom = "label",  # labels not text
               label.padding = unit(0.15, "lines"), # amount of padding around the labels
               label.size = 0) + # size of label border
    theme(legend.position = c(0.5,0.2),
         legend.title = element_blank(), # no title
         legend.key = element_blank()) # no keys
    dev.off()
  cat("successfully printed parsimoney tree\n",file="log/ksnp/drawkSNPtree.log",sep="",append=TRUE)

  tr=nj
  pdf(snakemake@output[["NJtree"]])#creates file
    p <- ggtree(tr) +
     xlim(0, 15) + # to allow more space for labels
     ggtitle("Neighbour joining tree from ksnp")#prints title
     p %<+% dd +  geom_tiplab(aes(fill = Type),
                 color = "black", # color for label font
                 geom = "label",  # labels not text
                 label.padding = unit(0.15, "lines"), # amount of padding around the labels
                 label.size = 0) + # size of label border
      theme(legend.position = c(0.5,0.2),
           legend.title = element_blank(), # no title
           legend.key = element_blank()) # no keys
      dev.off()
    cat("successfully printed meighbour joining  tree\n",file="log/ksnp/drawkSNPtree.log",sep="",append=TRUE)

    tr=ml
    pdf(snakemake@output[["NJtree"]])#creates file
      p <- ggtree(tr) +
       xlim(0, 15) + # to allow more space for labels
       ggtitle("Maximum Likelihood joining tree from ksnp")#prints title
       p %<+% dd +  geom_tiplab(aes(fill = Type),
                   color = "black", # color for label font
                   geom = "label",  # labels not text
                   label.padding = unit(0.15, "lines"), # amount of padding around the labels
                   label.size = 0) + # size of label border
        theme(legend.position = c(0.5,0.2),
             legend.title = element_blank(), # no title
             legend.key = element_blank()) # no keys
        dev.off()
      cat("successfully Maximum Likelihood joining  tree\n",file="log/ksnp/drawkSNPtree.log",sep="",append=TRUE)
