#!/usr/bin/env Rscript

## Read args
args = commandArgs(trailingOnly=TRUE)

if (length(args) == 0){
  stop("No arguments supplied",call.=FALSE)
}

# Load packages
require(writexl)
require(dplyr)
require(reshape2)
## Define the input file
input_file <- args[1]
output_file <- args[2]

print(paste("output: ",output_file))

inputDF <- read.delim(input_file,header=FALSE)
colnames(inputDF) <- c("ID","clermont")

## Create MGS File
##output_file <- "results/clermont/ezclermont_mqc.txt"
cat("# title: 'EZClermont'","\n",file=output_file)
cat("# section: ''","\n",file=output_file,append=T)
cat("# format: 'tsv'","\n",file=output_file,append=T)
cat("# plot_type: 'table'","\n",file=output_file,append=T)
suppressWarnings(write.table(inputDF,output_file,row.names=F,quote=F,sep="\t",append=T))

