#!/bin/sh
# Uncompress fastq.gz and fastq.bz2 files

file=$1
ext=${file##*.};

if [[ $ext == "gz" ]]; then
   exec gzip -cd "$@" 
else
  exec bzip2 -cd "$@"
fi
