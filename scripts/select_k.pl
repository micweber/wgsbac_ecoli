#!/usr/bin/perl
# Author: Jörg Linde
#
# selects optimal K from Kchooser.report
use Getopt::Long qw(GetOptions);


my $usage="does first step of kSNP analysis: MakeKSNP3infile,MakeFasta,Kchooser\n
selects optimal K\n
Usage: $0 --report \n Where:\n
--results Kchooser.report\n
";
$help="";
    GetOptions(
        'report|r=s' => \$report,
        'help|h=s' => \$help,
    ) or die ("$usage");



if($help ne "") {
  die ("$usage");

}

open FILE, $report or die $!;#opens input file kchoser report
while (my $line=<FILE>){  #reads table per line
        chop($line);
        if($line =~ m/The optimum value of K is /){#finds  for line The optimum value of K is
          @values = split('\s', $line);#splits at " "
          $k=$values[-1];# takes last entry
          $k = substr $k, 0, -1;#and removes "."
        }
}
print "$k\n"
