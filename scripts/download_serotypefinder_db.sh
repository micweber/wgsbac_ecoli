#!/bin/bash

## Download O_type.fsa
wget https://bitbucket.org/genomicepidemiology/serotypefinder_db/raw/25ddd141d245db6382ca5876f7c7ddd0288aeb30/O_type.fsa -P results/abricatedb/serotypefinder/
## Download H_type.fsa
wget https://bitbucket.org/genomicepidemiology/serotypefinder_db/raw/25ddd141d245db6382ca5876f7c7ddd0288aeb30/H_type.fsa -P results/abricatedb/serotypefinder/
## Prepare the BLAST directory for abricate
cat results/abricatedb/serotypefinder/H_type.fsa results/abricatedb/serotypefinder/O_type.fsa > results/abricatedb/serotypefinder/sequences
## Generate BLAST
makeblastdb -in results/abricatedb/serotypefinder/sequences -title serotypefinder -dbtype nucl -hash_index