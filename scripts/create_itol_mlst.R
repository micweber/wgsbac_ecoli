#!/usr/bin/env Rscript

#logs all output of R into snakemake log
log=file(snakemake@log[[1]],open="wt")
sink(log)
sink(log, type = "message")

require(dplyr)

## MLST Data Text output file
itolFile <- snakemake@output[["itol"]]

mlstDF <- read.delim(snakemake@input[["mlst"]],sep="\t", stringsAsFactors = FALSE)
colnames(mlstDF)[1] <- "ID"
mlstDF <- mlstDF <- mlstDF %>% select(ID,Type) %>% as.data.frame()
mlstDF$pos <- "-1"
mlstDF$color <- "#000000"
mlstDF$style <- "normal"
mlstDF$sizeFactor <- "1"
mlstDF$rotation <- "0"
mlstDF$Type[is.na(mlstDF$Type)] <- ""
#9606,Homo sapiens,-1,#ff0000,bold,2,0

cat("DATASET_TEXT","\n",file=itolFile)
cat("SEPARATOR TAB","\n",file=itolFile,append=T)
cat("COLOR","#ff0000","\n",sep="\t",file=itolFile,append=T)
cat("DATASET_LABEL","MLST","\n",sep="\t",file=itolFile,append=T)
cat("MARGIN","20","\n",sep="\t",file=itolFile,append=T)
cat("DATA","\n",file=itolFile,append=T)
write.table(mlstDF,itolFile,row.names=F,quote=F,sep="\t",append=T)
