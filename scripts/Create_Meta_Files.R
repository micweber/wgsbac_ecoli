
source("~/SourcesStandard/Pipeline/pipeline_start.R")

## Belen samples are E20E0490, E20E0503, E20E0539, E20E0583
setwd("~/AGGenomes/2021_08_18_RKI_MausSchaf/")

## 1 ## Generate a meta data table with columns forward and reverse reads
datadir <- "/data/AGr260/genomes/2021_08_18_RKI_Maus_Schaf/"

# NG-26447_19E0379_lib443656_7139_2_1.fastq.gz
## Try if the substitution works
gsub("[^_]+_[^_]+_([^_]+).*","\\1","O76_H19_18-00244-1_S28_L001_R1_001.fastq.gz")
filesDF <- ngs.generateMeta(datadir,patternId="[^_]+_[^_]+_([^_]+).*")
# metaDF[,1] <- paste("E",metaDF[,1],sep="")
# metaDF[,2] <- paste("E",metaDF[,2],sep="")
filesDF <- filesDF[1:2,]

referenceDF <- c("MG1655","MG1655","reference","","","/home/michael.weber/AG260/genomes/K12_MG1655/sequence.fasta","")
filesDF <- rbind(filesDF,referenceDF)
write.table(filesDF,file="workflow/genomes_files.csv",row.names=F,col.names=T,sep="\t")

## Write also a genomes meta table
metaDF <- filesDF[,1,drop=FALSE]
write.table(metaDF,file="workflow/genomes_meta.csv",row.names=F,col.names=T,sep="\t")

stopifnot(all(filesDF$ID %in% metaDF$ID))
