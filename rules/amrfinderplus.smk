


"""
Download AMRfinderDB
"""

rule downloadamrfinder:
    output:
        db="results/amrfinderplus/dbdownload.txt",
    log:
        "log/amrfinderplus/downloadamrfinder.log"
    benchmark:
        "log/benchmark/amrfinderplus/downloadamrfinder.txt"
    conda:
        "../envs/amrfinder.yaml"
    shell:
        "amrfinder -u   1> {output.db} 2>{log}"


"""
Uses AMRfinderplus to detect AMR genes
"""

rule amrfinderplus:
    input:
        fasta="results/finalAssembly/{sample}.fasta",
        db="results/amrfinderplus/dbdownload.txt"
    output:
        amrfinder_res="results/amrfinderplus/{sample}.tsv",
    log:
        "log/amrfinderplus/{sample}.log"
    benchmark:
        "log/benchmark/amrfinderplus/{sample}.txt"
    conda:
        "../envs/amrfinder.yaml"
    threads: 4
    params:
        genus=config.get("genus","organism")
    shell:
      """
      if [[ '{params.genus}' == 'Escherichia' ]];
      then 
        amrfinder --threads {threads} -n {input.fasta} --plus  1> {output.amrfinder_res} 2>{log}
      else 
        amrfinder --threads {threads} -n {input.fasta} --plus  1> {output.amrfinder_res} 2>{log}
      fi
      """



rule collect_amrfinderplus:
    input:
        amrfiles=expand("results/amrfinderplus/{sample}.tsv", sample=samples.ID),
        metadata=config['metadata']
    output:
        alltsv="results/amrfinderplus/allAmrfinder.tsv",
        allxls="results/amrfinderplus/allAmrfinder.xlsx",
        table="results/amrfinderplus/all_amrfinder_table.csv",
        multiqc="results/amrfinderplus/AMRfinder_mqc.txt"
    log:
        "log/amrfinderplus/collect_amrfinderplus.log"
    benchmark:
        "log/benchmark/amrfinderplus/collect_amrfinderplus.txt"
    conda:
        "../envs/rxls.yaml"
    script:
        "../scripts/combine_amrfinder_V2.R"
