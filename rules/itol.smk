"""
ITOL: Create ITOL template files for tree annotation
"""

rule itol_create_all:
  input:
    "results/itol/itol_datatext_mlst.txt",
    "results/itol/itol_datatext_sero.txt",
    "results/itol/itol_binary_amrgenes.txt"
  output:
    "results/itol/itol_finished"
  shell:
    "touch {output}"
    
    
rule itol_create_amrfinder:
  input:
    amrfinder="results/amrfinderplus/all_amrfinder_table.csv"
  output:
    itol="results/itol/itol_binary_amrgenes.txt"
  log:
    "log/itol/itol_amrfinder.txt"
  conda:
    "../envs/rcran.yaml"
  params:
    patterns=config.get("itol_amr","blaCTX,gyrA,tet")
  script:
    "../scripts/create_itol_amrfinder.R"
    

rule itol_create_mlst:
  input:
    mlst="results/mlst/allMLST.csv"
  output:
    itol="results/itol/itol_datatext_mlst.txt"
  log:
    "log/itol/itol_mlst.txt"
  conda:
    "../envs/rcran.yaml"
  script:
    "../scripts/create_itol_mlst.R"
    
  
  
rule itol_create_serofinder:
  input:
    sero="results/abricate/all_serofinder.tsv"
  output:
    itol="results/itol/itol_datatext_sero.txt"
  log:
    "log/itol/itol_sero.txt"
  conda:
    "../envs/rcran.yaml"
  script:
    "../scripts/create_itol_serofinder.R"

    