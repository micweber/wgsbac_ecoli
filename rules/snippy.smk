"""2019.03.29 MYA"""


"""
snippy
Runs snippy for each fastq file
"""
rule snippy:
    input:#receive all forward and reverse fastq files
        fw= unzip_res+"{fastq}_R1.fastq",#unzipped fastq files
        rv= unzip_res+"{fastq}_R2.fastq",
        ref=config["refstrain"]#reference
    output:
        snippy= directory(snippy_folder+"{fastq}"),
    threads:
        16
    benchmark:
        snippy_bench+"{fastq}.txt"
    log:
        snippy_log+"{fastq}.log"
    conda:
      "../envs/snippy.yaml"
    shell:
        "snippy --force  --cpus {threads} --outdir {output.snippy} --ref {input.ref} --R1 {input.fw} --R2 {input.rv} 2>{log}"

"""
snippy_core
Combines snippy into multiple  alignment
"""
rule snippy_core:
    input:
        snippylocal=expand( snippy_folder + "{fastq}", fastq=fastq),#snippy results from each single fastq files
        ref=config["refstrain"]
    benchmark:
        snippy_bench+"snippy_core.txt"
    log:
        snippy_log+"snippy_core.log"
    output:#all output from snippy-core
        aln= snippy_core+"core.aln",
        full= snippy_core+"core.full.aln",
        ref= snippy_core+"core.ref.fa",
        tab= snippy_core+"core.tab",
        txt= snippy_core+"core.txt",
        vcf= snippy_core+"core.vcf",
    conda: "../envs/snippy.yaml"
    shell:#use prefix and store results in tmp directory, afterwards move them to final directory
        "snippy-core  --ref {input.ref} {input.snippylocal} --prefix tmp  2>{log}&&\
        mv tmp.aln {output.aln}&&\
        mv tmp.full.aln {output.full}&&\
        mv tmp.ref.fa {output.ref}&&\
        mv tmp.tab {output.tab}&&\
        mv tmp.txt {output.txt}&&\
        mv tmp.vcf {output.vcf}"


"""
snpdist
Convert a core alignment from snippy to SNP distance matrix
"""
rule snpdist:
    input:
        aln= snippy_core+"core.aln",
    benchmark:
        snippy_bench+"snpdist.txt"
    log:
        snippy_log+"snpdist.log"
    output:#all output from snippy-core
        snpdist= snippy_core+"snpdist.csv",

    conda: "../envs/snpdist.yaml"
    shell:
        "snp-dists {input.aln} > {output.snpdist} 2>{log}"


"""
fasttree
Runs fasttree on mutliple sequence alignment from snippy
Returns tree with IDs in nodes as newick
"""
rule fasttree:
    input:
        aln= snippy_core+"core.aln"
    benchmark:
        snippy_bench+"snippy_fasttree.txt"
    log:
        snippy_log+"snippy_fasttree.log"
    output:
        tree=(tmp+"snippy_fasttree.nw")
    conda: "../envs/fasttree.yaml"
    shell:
        "FastTree -gtr -nt {input.aln} > {output.tree} 2>{log}"


"""
Draws tree from snippy results
Uses metadata of KNOWN samples (typestrains etc) combined with newly analzed samples
"""

rule drawSnippytree:
    input:
        metadata=config['metadata'],#metdada of known reference samples (typestrains,...)
        #Always us NEwick as variable name
        newick=tmp+"snippy_fasttree.nw",#tree from fasttree in newick fromat
    output:
        #Always use pdf as variable name
        pdf=trees+"snippy_fasttree.pdf",
        newick=trees+"snippy_fasttree.nw"#tree in newick format with exchanded IDs into common names
    params:
        #alsways use params as variable name
        title="Phylogenetic tree from Snippy and Fasttree"
#    conda:
#        "../envs/rcran.yaml"
    log:
        snippy_log+"drawSnippytree.log"
    benchmark:
        snippy_bench+"drawSnippytree.txt"
    script:
        "../scripts/plottree.R"
