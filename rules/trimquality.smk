rule quality_control:
  input:
    fw="input/{fastq}_R1.fastq.gz",
    rv="input/{fastq}_R2.fastq.gz"
  output:
    directory("input_trimmed/{fastq}")
  threads: 4
  params:
    outdir="input_trimmed/{fastq}"
  shell:
    """
		trim_galore --paired --phred33 --cores {threads} --fastqc -q 30 --length 100 -o {params.outdir} {input.fw} {input.rv}
    """