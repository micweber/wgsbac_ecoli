
"""
Prokka for gene prediction and annotation

"""
rule prokka:
    input:
        "results/finalAssembly/{sample}.fasta"
    output:
        directory("results/prokka/{sample}")
    threads:
        8
    benchmark:
        "log/benchmark/prokka/{sample}.txt"
    log:
        "log/prokka/{sample}.txt"
    conda:
        "../envs/prokka.yaml"
    params:
        prefix="{sample}",
        gcode=config["genetic_code"],
        outdir="results/prokka/{sample}"
    shell:
        """
        prokka --cpus {threads}  --force --kingdom Bacteria --gcode {params.gcode}\
            --outdir {params.outdir}\
            --prefix {params.prefix} \
            {input} &> {log}
        """