"""
Assembly of the reads using shovill.
"""
#input: receive all forward and reverse fastq files
#output: is directoy for each sample

#18-00244-1_R1_val_1.fq.gz
#input/{fastq}_R1.fastq.gz

## Trimmed
## input_trimmed/{fastq}/{fastq}_R1_val_1.fq.gz

rule shovill:
    input:
        fw="input/{fastq}_R1.fastq.gz",
        rv="input/{fastq}_R2.fastq.gz"
    output:
        directory("results/shovill/{fastq}")
    threads:
        16
    log:
        "log/shovill/{fastq}.log"
    conda:
        "../envs/shovill.yaml"
    group: "assembly"
    shell:
        "shovill --cpus {threads} \
            --R1 {input.fw} \
            --R2 {input.rv}\
            --outdir {output}\
            2> {log}"


"""
Renames contigs to have short contig names (required by some tools)
"""
#input is result from shovill


rule shovill_rename_contigs:
    input:
        "results/shovill/{sample}",
    output:
        "results/finalAssembly/{sample}.fasta"
    log:
        "log/rename/{sample}.log"
    benchmark:
        "log/rename/{sample}_benchmark.txt"
    shell:
        "sed -r 's/(>[^ ]+) .*/\\1/g' {input}/contigs.fa > {output} 2> {log}"

